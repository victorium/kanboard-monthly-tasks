<?php

namespace Kanboard\Plugin\MonthlyTasks;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        $this->route->addRoute('/monthly/tasks', 'MyController', 'monthly', 'monthlytasks');
        $this->template->hook->attach('template:dashboard:sidebar', 'monthlytasks:dashboard/sidebar');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__ . '/Locale');
    }

    public function getPluginName()
    {
        return 'Monthly Tasks';
    }

    public function getPluginDescription()
    {
        return t('Show monthly time assigned to tasks per user');
    }

    public function getPluginAuthor()
    {
        return 'Joseph N. Mutumi';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://bitbucket.org/victorium/kanboard-monthly-tasks';
    }
}
