<div class="page-header">
    <h2><?= t('Tasks in month for:') ?> <?= $date ?></h2>
    <form class="form-inline" autocomplete="off">
        <input type="hidden" name="controller" value="MyController" />
        <input type="hidden" name="action" value="monthly" />
        <input type="hidden" name="plugin" value="monthlytasks" />
        <?= $helper->form->date(t('For Date'), 'for_date', ['for_date' => $date], [], []); ?>
        <?php if ($isAdmin) {
            echo $helper->form->label(t('For User'),  'user_id') .
                $helper->form->select('user_id', $users, ['user_id' => $user['id']], []);
        }
        ?>
        <?= $this->modal->submitButtons(['submitLabel' => 'Search']) ?>
    </form>
    <br />
    <table class="table-striped table-scrolling">
        <thead>
            <tr>
                <th class="column-20"><?= t('Project') ?></th>
                <th class="column-40"><?= t('Task') ?></th>
                <th class="column-10"><?= t('Column') ?></th>
                <th class="column-10"><?= t('Closed') ?></th>
                <th class="column-10"><?= t('Closed Hours') ?></th>
                <th class="column-10"><?= t('Open Hours') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $totalClosed = 0;
            $totalOpen = 0;
            foreach ($tasks as $task) :
                preg_match("/\((\d+) hours\)/", $task['title'], $matches);
                $hours = (int)$matches[1];
                $isClosed = $task['is_active'] == '0';

                $totalClosed += $isClosed ? $hours : 0;
                $totalOpen += $isClosed ? 0 : $hours;
            ?>
                <tr>
                    <td><?= $this->url->link($task['project_name'], 'BoardViewController', 'show', ['project_id' => $task['project_id']]) ?></td>
                    <td><?= $this->url->link($task['title'], 'TaskViewController', 'show', ['project_id' => $task['project_id'], 'task_id' => $task['id']]) ?></td>
                    <td><?= $this->text->e($task['column_name']) ?></td>
                    <td><?= $isClosed ? t("Yes") : t("No") ?></td>
                    <td><?= $isClosed ? $hours : "" ?></td>
                    <td><?= $isClosed ? "" : $hours ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">TOTAL</td>
                <td><?= $totalClosed ?></td>
                <td><?= $totalOpen ?></td>
            </tr>
        </tfoot>
    </table>
</div>