<?php

namespace Kanboard\Plugin\MonthlyTasks\Controller;

use DateTime;
use Kanboard\Controller\BaseController;

class MyController extends BaseController
{
    public function monthly()
    {
        $isAdmin = $this->userSession->isAdmin();

        $date = new DateTime($this->request->getStringParam('for_date', "now"));
        $date = $date->format("m/d/Y");

        if ($isAdmin) {
            $user = $this->getUser();
            $tmp = $this->userModel->getAll();
            $users = [];
            foreach ($tmp as $u) {
                $users[$u['id']] = $this->helper->user->getFullname($u);
            }
        } else {
            $user = $this->userModel->getById($this->userSession->getId());
            $users = [];
        }

        $start = new DateTime("first day of $date");
        $end = new DateTime("last day of $date");
        $tasks = $this->taskFinderModel
            ->getUserQuery($user['id'])
            ->gte('date_due', $start->getTimestamp())
            ->lte('date_due', $end->getTimestamp())
            ->findAll();
        $this->response->html($this->helper->layout->dashboard('monthlytasks:dashboard/tasks', array(
            'title' => t('Monthly Tasks for %s', $this->helper->user->getFullname($user)),
            'tasks' => $tasks,
            'helper' => $this->helper,
            'date' => $date,
            'user' => $user,
            'users' => $users,
            'isAdmin' => $isAdmin,
        )));
    }
}
